/* Script for homeword */

/* Create database */
CREATE DATABASE ebay_database;

/* Create tables*/
/* Categories */
CREATE TABLE Categories (
	"Id" uuid NOT NULL,
	"Name" varchar(100) NOT NULL,
	CONSTRAINT "PK_Categories" PRIMARY KEY ("Id")
);

/* Details */
CREATE TABLE ProductsInfo (
	"Id" uuid NOT NULL,
	"Type" int4 NOT NULL,
	"Info" varchar(500) NOT NULL,
	"ProductId" uuid NOT NULL,
	CONSTRAINT "PK_ProductsInfo" PRIMARY KEY ("Id")
);

/* Products */
CREATE TABLE Products (
	"Id" uuid NOT NULL,
	"Name" varchar(100) NOT NULL,
	"Price" numeric NOT NULL,
	"CategoryId" uuid NOT NULL,
	CONSTRAINT "PK_Products" PRIMARY KEY ("Id")
);

/* Data */
/* Categories */
INSERT INTO Categories
("Name")
VALUES('Electronics, Toys, Home & Garden, Clothing, Books');