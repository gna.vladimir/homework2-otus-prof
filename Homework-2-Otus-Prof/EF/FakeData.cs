﻿using Homework_2_Otus_Prof.Domain;

namespace Homework_2_Otus_Prof.EF
{
    public static class FakeData
    {
        private static List<string> _colors = new() { "Black", "White", "Red", "Cyan", "Violet", "Green", "Blue", "Brown" };

        public static List<Product> Products { get; }

        public static List<ProductsInfo> ProductInfos { get; }

        public static List<Category> Categories { get; }


        static FakeData()
        {
            var rnd = new Random();
            Categories = new()
            {
                new Category() {Id = Guid.Parse("4be82e70-7469-4832-ac76-e6192d7ccc9f"), Name = "Electronics"},
                new Category() {Id = Guid.Parse("a6ed9226-7002-4b8d-92c8-687dc296c44f"), Name = "Clothing"},
                new Category() {Id = Guid.Parse("96147f3c-aa6e-4276-bf09-5c2973676cdb"), Name = "Home & Garden"},
                new Category() {Id = Guid.Parse("668fd565-a369-44c5-a75f-490487587adf"), Name = "Toys"},
                new Category() {Id = Guid.Parse("b6100178-ef22-4631-b66a-8cb761667efb"), Name = "Books"}
            };

            Products = new()
            {
                new Product() {Id = Guid.Parse("1c030a7d-137b-4399-97a5-c6e0af6cdc5e"), Name = "Smartphone", Price = rnd.Next(10,150), CategoryId = Guid.Parse("4be82e70-7469-4832-ac76-e6192d7ccc9f")},
                new Product() {Id = Guid.Parse("62d946e1-f2e0-439f-ae3c-653854fdffee"), Name = "T-shirt", Price = rnd.Next(10,150), CategoryId = Guid.Parse("a6ed9226-7002-4b8d-92c8-687dc296c44f")},
                new Product() {Id = Guid.Parse("02b6b57a-899f-4549-8f9d-f4f71b7f803b"), Name = "Coffee Maker", Price = rnd.Next(10,150), CategoryId = Guid.Parse("96147f3c-aa6e-4276-bf09-5c2973676cdb")},
                new Product() {Id = Guid.Parse("0ea04d2b-7d40-4417-835a-59e75c9e23ee"), Name = "Action Figure", Price = rnd.Next(10,150), CategoryId = Guid.Parse("668fd565-a369-44c5-a75f-490487587adf")},
                new Product() {Id = Guid.Parse("cc7c379b-f857-4e70-84ca-e0698de70594"), Name = "Novel", Price = rnd.Next(10,150), CategoryId = Guid.Parse("b6100178-ef22-4631-b66a-8cb761667efb")},
            };

            ProductInfos = new()
            {
                new ProductsInfo() {Id = Guid.Parse("256bf9ef-b16d-4c14-b809-e7883e5b34df"), ProductId = Guid.Parse("1c030a7d-137b-4399-97a5-c6e0af6cdc5e"), Type = DetailsType.Color, Info = _colors[rnd.Next(0,_colors.Count-1)]},
                new ProductsInfo() {Id = Guid.Parse("7847aa94-aed9-4292-a87e-18cf3825ccf8"), ProductId = Guid.Parse("1c030a7d-137b-4399-97a5-c6e0af6cdc5e"), Type = DetailsType.Weight, Info = rnd.Next(10,150).ToString()},
                new ProductsInfo() {Id = Guid.Parse("193d5a27-2175-47b5-a022-e019ed828b68"), ProductId = Guid.Parse("1c030a7d-137b-4399-97a5-c6e0af6cdc5e"), Type = DetailsType.Width, Info = rnd.Next(10,150).ToString()},

                new ProductsInfo() {Id = Guid.Parse("0155e022-2896-4653-902b-bd9088ee0add"), ProductId = Guid.Parse("62d946e1-f2e0-439f-ae3c-653854fdffee"), Type = DetailsType.Color, Info = _colors[rnd.Next(0,_colors.Count-1)]},
                new ProductsInfo() {Id = Guid.Parse("26a8b250-d802-42af-a9e6-8a3b6d74d2b8"), ProductId = Guid.Parse("62d946e1-f2e0-439f-ae3c-653854fdffee"), Type = DetailsType.Weight, Info = rnd.Next(10,150).ToString()},
                new ProductsInfo() {Id = Guid.Parse("55e7ca0b-6d14-4033-81ea-d784d815f7ff"), ProductId = Guid.Parse("62d946e1-f2e0-439f-ae3c-653854fdffee"), Type = DetailsType.Width, Info = rnd.Next(10,150).ToString()},

                new ProductsInfo() {Id = Guid.Parse("d61a4154-f028-4a41-a872-071456536acc"), ProductId = Guid.Parse("02b6b57a-899f-4549-8f9d-f4f71b7f803b"), Type = DetailsType.Color, Info = _colors[rnd.Next(0,_colors.Count-1)]},
                new ProductsInfo() {Id = Guid.Parse("85d3def6-90d5-423b-9e7a-29f6e79f4577"), ProductId = Guid.Parse("02b6b57a-899f-4549-8f9d-f4f71b7f803b"), Type = DetailsType.Weight, Info = rnd.Next(10,150).ToString()},
                new ProductsInfo() {Id = Guid.Parse("2fddf985-a0bc-4db2-83b5-18cac6588bcb"), ProductId = Guid.Parse("02b6b57a-899f-4549-8f9d-f4f71b7f803b"), Type = DetailsType.Width, Info = rnd.Next(10,150).ToString()},

                new ProductsInfo() {Id = Guid.Parse("70c54eda-7e0e-4838-b1e8-0967e823cdfc"), ProductId = Guid.Parse("0ea04d2b-7d40-4417-835a-59e75c9e23ee"), Type = DetailsType.Color, Info = _colors[rnd.Next(0,_colors.Count-1)]},
                new ProductsInfo() {Id = Guid.Parse("740e10c3-b052-4d40-91df-95236540f224"), ProductId = Guid.Parse("0ea04d2b-7d40-4417-835a-59e75c9e23ee"), Type = DetailsType.Weight, Info = rnd.Next(10,150).ToString()},
                new ProductsInfo() {Id = Guid.Parse("6c81813e-02f6-493b-8d02-80e520ce82a0"), ProductId = Guid.Parse("0ea04d2b-7d40-4417-835a-59e75c9e23ee"), Type = DetailsType.Width, Info = rnd.Next(10,150).ToString()},

                new ProductsInfo() {Id = Guid.Parse("28d84652-6ecb-408d-aa4b-3852fcacfcf4"), ProductId = Guid.Parse("cc7c379b-f857-4e70-84ca-e0698de70594"), Type = DetailsType.Color, Info = _colors[rnd.Next(0,_colors.Count-1)]},
                new ProductsInfo() {Id = Guid.Parse("c916f0c0-4b23-44fa-99e1-601f56a1514e"), ProductId = Guid.Parse("cc7c379b-f857-4e70-84ca-e0698de70594"), Type = DetailsType.Weight, Info = rnd.Next(10,150).ToString()},
                new ProductsInfo() {Id = Guid.Parse("56b57fa5-570a-41b4-8158-7e4888c51c1f"), ProductId = Guid.Parse("cc7c379b-f857-4e70-84ca-e0698de70594"), Type = DetailsType.Width, Info = rnd.Next(10,150).ToString()},
            };
        }
    }
}
