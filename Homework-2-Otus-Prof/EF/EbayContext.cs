﻿using Homework_2_Otus_Prof.Domain;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace Homework_2_Otus_Prof.EF
{
    public class EbayContext : DbContext
    {
        public DbSet<ProductsInfo> ProductsInfo { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }

        public EbayContext(DbContextOptions options) 
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {            
            modelBuilder.Entity<Category>().HasData(FakeData.Categories);

            modelBuilder.Entity<Product>().HasData(FakeData.Products);

            modelBuilder.Entity<ProductsInfo>().HasData(FakeData.ProductInfos);
        }
    }
}
