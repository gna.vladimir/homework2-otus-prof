﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Homework_2_Otus_Prof.EF.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Price = table.Column<decimal>(type: "numeric", nullable: false),
                    CategoryId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductsInfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false),
                    Info = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductsInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductsInfo_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("4be82e70-7469-4832-ac76-e6192d7ccc9f"), "Electronics" },
                    { new Guid("668fd565-a369-44c5-a75f-490487587adf"), "Toys" },
                    { new Guid("96147f3c-aa6e-4276-bf09-5c2973676cdb"), "Home & Garden" },
                    { new Guid("a6ed9226-7002-4b8d-92c8-687dc296c44f"), "Clothing" },
                    { new Guid("b6100178-ef22-4631-b66a-8cb761667efb"), "Books" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Name", "Price" },
                values: new object[,]
                {
                    { new Guid("02b6b57a-899f-4549-8f9d-f4f71b7f803b"), new Guid("96147f3c-aa6e-4276-bf09-5c2973676cdb"), "Coffee Maker", 63m },
                    { new Guid("0ea04d2b-7d40-4417-835a-59e75c9e23ee"), new Guid("668fd565-a369-44c5-a75f-490487587adf"), "Action Figure", 99m },
                    { new Guid("1c030a7d-137b-4399-97a5-c6e0af6cdc5e"), new Guid("4be82e70-7469-4832-ac76-e6192d7ccc9f"), "Smartphone", 32m },
                    { new Guid("62d946e1-f2e0-439f-ae3c-653854fdffee"), new Guid("a6ed9226-7002-4b8d-92c8-687dc296c44f"), "T-shirt", 59m },
                    { new Guid("cc7c379b-f857-4e70-84ca-e0698de70594"), new Guid("b6100178-ef22-4631-b66a-8cb761667efb"), "Novel", 101m }
                });

            migrationBuilder.InsertData(
                table: "ProductsInfo",
                columns: new[] { "Id", "Info", "ProductId", "Type" },
                values: new object[,]
                {
                    { new Guid("0155e022-2896-4653-902b-bd9088ee0add"), "Cyan", new Guid("62d946e1-f2e0-439f-ae3c-653854fdffee"), 0 },
                    { new Guid("193d5a27-2175-47b5-a022-e019ed828b68"), "107", new Guid("1c030a7d-137b-4399-97a5-c6e0af6cdc5e"), 2 },
                    { new Guid("256bf9ef-b16d-4c14-b809-e7883e5b34df"), "White", new Guid("1c030a7d-137b-4399-97a5-c6e0af6cdc5e"), 0 },
                    { new Guid("26a8b250-d802-42af-a9e6-8a3b6d74d2b8"), "18", new Guid("62d946e1-f2e0-439f-ae3c-653854fdffee"), 1 },
                    { new Guid("28d84652-6ecb-408d-aa4b-3852fcacfcf4"), "Black", new Guid("cc7c379b-f857-4e70-84ca-e0698de70594"), 0 },
                    { new Guid("2fddf985-a0bc-4db2-83b5-18cac6588bcb"), "49", new Guid("02b6b57a-899f-4549-8f9d-f4f71b7f803b"), 2 },
                    { new Guid("55e7ca0b-6d14-4033-81ea-d784d815f7ff"), "143", new Guid("62d946e1-f2e0-439f-ae3c-653854fdffee"), 2 },
                    { new Guid("56b57fa5-570a-41b4-8158-7e4888c51c1f"), "11", new Guid("cc7c379b-f857-4e70-84ca-e0698de70594"), 2 },
                    { new Guid("6c81813e-02f6-493b-8d02-80e520ce82a0"), "84", new Guid("0ea04d2b-7d40-4417-835a-59e75c9e23ee"), 2 },
                    { new Guid("70c54eda-7e0e-4838-b1e8-0967e823cdfc"), "Cyan", new Guid("0ea04d2b-7d40-4417-835a-59e75c9e23ee"), 0 },
                    { new Guid("740e10c3-b052-4d40-91df-95236540f224"), "60", new Guid("0ea04d2b-7d40-4417-835a-59e75c9e23ee"), 1 },
                    { new Guid("7847aa94-aed9-4292-a87e-18cf3825ccf8"), "120", new Guid("1c030a7d-137b-4399-97a5-c6e0af6cdc5e"), 1 },
                    { new Guid("85d3def6-90d5-423b-9e7a-29f6e79f4577"), "16", new Guid("02b6b57a-899f-4549-8f9d-f4f71b7f803b"), 1 },
                    { new Guid("c916f0c0-4b23-44fa-99e1-601f56a1514e"), "98", new Guid("cc7c379b-f857-4e70-84ca-e0698de70594"), 1 },
                    { new Guid("d61a4154-f028-4a41-a872-071456536acc"), "Black", new Guid("02b6b57a-899f-4549-8f9d-f4f71b7f803b"), 0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductsInfo_ProductId",
                table: "ProductsInfo",
                column: "ProductId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductsInfo");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
