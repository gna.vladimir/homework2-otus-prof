﻿using System.ComponentModel.DataAnnotations;

namespace Homework_2_Otus_Prof.Domain
{
    public class ProductsInfo : BaseEntity
    {
        [EnumDataType(typeof(DetailsType))]
        public DetailsType Type { get; set; }

        [MaxLength(500)]
        public string Info { get; set; }

        [Required]
        public Guid ProductId { get; set; }

        public Product Product { get; set; }
    }

    public enum DetailsType
    {
        Color,
        Weight,
        Width
    }
}
