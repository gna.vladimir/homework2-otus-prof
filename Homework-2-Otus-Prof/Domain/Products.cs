﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Homework_2_Otus_Prof.Domain
{
    public class Product : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public decimal Price { get; set; }

        [Required]
        public Guid CategoryId { get; set; }

        public Category Category { get; set; } 

        public List<ProductsInfo> Details { get; set; } 
    }
}
