﻿using System.ComponentModel.DataAnnotations;

namespace Homework_2_Otus_Prof.Domain
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}
