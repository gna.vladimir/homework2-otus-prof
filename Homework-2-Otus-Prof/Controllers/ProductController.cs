using Homework_2_Otus_Prof.Domain;
using Homework_2_Otus_Prof.EF;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Homework_2_Otus_Prof.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly EbayContext _context;

        public ProductController(EbayContext context)
        {
            _context = context;
        }

        [HttpGet(Name = "GetAllProducts")]
        public async Task<ActionResult<ICollection<Product>>> Get()
        {
            var products = await _context.Products
                .Include(x => x.Details).AsNoTracking()                
                .ToListAsync();
            return Ok(products);
        }

        [HttpPost]
        public async Task<ActionResult<Product>> CreateProduct([FromBody] Product product)
        {
            _context.Add(product);
            await _context.SaveChangesAsync();
            return CreatedAtAction(nameof(CreateProduct), product);
        }
    }
}