using Homework_2_Otus_Prof.Domain;
using Homework_2_Otus_Prof.EF;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Homework_2_Otus_Prof.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DetailsController : ControllerBase
    {
        private readonly EbayContext _context;

        public DetailsController(EbayContext context)
        {
            _context = context;
        }

        [HttpGet(Name = "GetInfo")]
        public async Task<ActionResult<ICollection<ProductsInfo>>> Get()
        {
            var productsInfos = await _context.ProductsInfo
                .Include(x => x.Product).AsNoTracking()                
                .ToListAsync();
            return Ok(productsInfos);
        }

        [HttpPost]
        public async Task<ActionResult<ProductsInfo>> CreateCategory([FromBody] ProductsInfo productsInfo)
        {
            _context.Add(productsInfo);
            await _context.SaveChangesAsync();
            return CreatedAtAction(nameof(CreateCategory), productsInfo);
        }
    }
}