using Homework_2_Otus_Prof.Domain;
using Homework_2_Otus_Prof.EF;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Homework_2_Otus_Prof.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly EbayContext _context;

        public CategoryController(EbayContext context)
        {
            _context = context;
        }

        [HttpGet(Name = "GetAllCategories")]
        public async Task<ActionResult<ICollection<Category>>> Get()
        {
            var categories = await _context.Categories
                .Include(x => x.Products).AsNoTracking()                
                .ToListAsync();
            return Ok(categories);
        }

        [HttpPost]
        public async Task<ActionResult<Category>> CreateCategory([FromBody] Category category)
        {
            _context.Add(category);
            await _context.SaveChangesAsync();
            return CreatedAtAction(nameof(CreateCategory), category);
        }
    }
}